all: 
	make -C bin
	cd ..
	make -C compiler

#builds a production version of pass (without some files that enable development 
# of the pass language itself) and puts it into a tarball	
build:
	cp -r ../pass ../.p/ 
	make -C ../.p/docs
	make clean -C ../.p/examples
	make clean -C ../.p/compiler
	rm -rf ../.p/Tests ../.p/node_modules ../.p/TODO ../.p/docs/*.md ../.p/docs/Makefile ../.p/.git* ../.p/*~ ../.p/examples/Makefile ../.p/compiler/*~
	mv ../.p/ ./pass
	tar cvzf ../pass.tar.gz pass
	rm -rf pass
	clear
	echo "pass.tar.gz is tared in ../pass.tar.gz" 
	
clean:
	make clean -C bin
	rm -rf examples/chat/*.js examples/logger/logger.js examples/multi-room-chat/mrc.js
	sudo rm -rf node_modules
	make clean -C compiler
