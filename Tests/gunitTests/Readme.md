# * ----------------------------------------------------------------------------- #
# * author: Andrew Lamping 								     #
# * Readme.md										     #
# * This file contains makefile instructions for Pass program/file testing.	     #
# *------------------------------------------------------------------------------ #

This file in this folder, TerminalTests.gunit, served to test the end terminal productions in the Pass grammar.
There are 101 tests, with each one resulting in passing at the moment.

To run these tests, run the following command:

$make clean
$make
$make tests
