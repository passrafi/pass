#!/bin/bash
# ---------------------------------------------------------------------------
# author: Rafael Castellanos
# dependencies.sh
# installs dependencies
# ------------------------------
OS=$(uname)

#test if java is installed
javac -version &> /dev/null
if ! [ $? -eq 0 ]; then
	echo
	echo "====================================================================="
	echo "      You must install the Java Development Kit (jdk6 or higher)"
	echo "  http://www.oracle.com/technetwork/java/javase/downloads/index.html"
	echo "======================================================================"
	echo
	exit 2
fi

#check if g++ is installed
g++ -v &> /dev/null
if ! [ $? -eq 0 ]; then
	echo
	echo "======================================================================"
	echo "                 You must install g++ c++ compiler"
	echo "                       http://gcc.gnu.org/"
	echo "======================================================================"
	echo
	exit 2
fi

#check if wget or curl is installed
wget -V &> /dev/null
if [ $? -eq 0 ]; then
	GRAB="wget"
else
	curl -V
	if [ $? -eq 0 ]
	then
		GRAB="curl"
	else
		echo 	
		echo "======================================================================"		
		echo "                   Can't configure dependencies"
		echo "  Install curl or wget to proceed http://curl.haxx.se/download.html"
		echo "======================================================================"
		echo
		exit 2
	fi
fi

#check if antlr is installed
java org.antlr.Tool -version &> /dev/null	
if ! [ $? -eq 0 ] 
then 
	clear
	sudo mkdir /usr/local/antlr -p
	#offline substitute
	pwd
	sudo cp ../lib/antlr/antlr-3.4-complete.jar /usr/local/antlr/
	pushd /usr/local/antlr
	sudo chmod 755 antlr-3.4-complete.jar
	
	#make classpath perminant
	env | grep /usr/local/antlr/antlr-3.4-complete.jar
	if ! [ $? -eq 0 ]; then
		export CLASSPATH=/usr/local/antlr/antlr-3.4-complete.jar:$CLASSPATH
		#test if install was success
		java org.antlr.Tool -version
		if ! [ $? -eq 0 ]; then
			echo "failed to install antlr" 
			exit 2
		fi
		
		ls /etc/bash.bashrc &> /dev/null
		if [ $? -eq 0 ]; then
			#system wide ubuntu install
			sudo sh -c "echo 'export CLASSPATH=/usr/local/antlr/antlr-3.4-complete.jar:$CLASSPATH' >> /etc/bash.bashrc"
		elif [ $OS == "Linux" ]; then
			#other os install (local )
			echo "export CLASSPATH=/usr/local/antlr/antlr-3.4-complete.jar:$CLASSPATH" >> ~/.bashrc
		elif [ $OS == "Darwin" ]; then
			echo "export CLASSPATH=/usr/local/antlr/antlr-3.4-complete.jar:$CLASSPATH" >> ~/.profile
		fi		
	fi
popd
echo
echo "===================================="
echo "... ANTLR installed successfully ..."
echo "===================================="
echo
fi

#check if node is installed
node --version &> /dev/null
if ! [ $? -eq 0 ] 
then 
	echo "========================="
	echo "... Installing NodeJS ..."
	echo "========================="
	pwd
		if [ $GRAB == "wget" ]; then
		wget http://nodejs.org/dist/v0.8.16/node-v0.8.16.tar.gz
		if ! [ $? -eq 0 ] 
		then
			echo "You must be connected to the internet to finish installing..."
			exit 2
		fi
	else
		curl http://nodejs.org/dist/v0.8.16/node-v0.8.16.tar.gz -o node-v0.8.16.tar.gz
		if ! [ $? -eq 0 ]; then
			echo "You must be connected to the internet to finish installing..."
			exit 2
		fi
	fi
	tar -xzf node-v0.8.16.tar.gz

	cd node-v0.8.16
	sudo ./configure
	sudo make -j3
	sudo make install -j3 
	cd ..
	sudo rm -rf node-v0.8.16
fi
echo
echo "==========================================="
echo "        All dependencies satisfied         "
echo "          ... Installing pass ...          "
echo "==========================================="
echo
