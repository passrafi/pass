Installing Pass
===============
##Download [pass.tar.gz](http://passlang.bitbucket.org/pass.tar.gz)
Open up a terminal

Change directories to where you downloaded pass.tar.gz

Type the following commands:

	tar -xzf pass.tar.gz
	cd pass
	make
	
Note: You must be connected to the Internet in order for the install to work.

##Depedencies
The install script will check for dependencies.
There are however a few dependencies that the install script does not handle. 
If in doubt just type `make` and you will find out which dependencies you have.

	JDK 1.6 or better
	gcc
	curl or wget
	make

On a mac you can satisfy all of these dependencies by going to the app store and
installing [Xcode](http://itunes.apple.com/us/app/xcode/id448457090?mt=12)

Open Xcode, then go to the menus:

Xcode->Preferences->Downloads and install component named "Command Line Tools"

On Linux, you can use your local package manager to install the depdencies if 
you don't already have them.

##Operating Systems
Pass works on Macintosh and Linux operating Systems.

There is not currently a version for Windows.
##Documentation
The Pass documentation is located [here](http://passlang.bitbucket.org/documentation/)
